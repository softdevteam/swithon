# Pulled from the shared test repo
from unipycation_shared.tests.base_test_microbench import BaseTestMicroBench
from pytest import skip

def skipmethod_broken(self):
    skip("This test is broken under swithon")

class TestMicroBench(BaseTestMicroBench): pass
