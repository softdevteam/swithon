import pytest

from swithon.c import C
from swithon import conversion
from swithon.objects import CoreTerm, Var
from debug import *

# -------------------------------
# Test the specific py_*_of_pro_*
# -------------------------------

def test_py_int_of_pro_int():
    pro_int = C.pl_int(666)
    py_int = conversion.py_int_of_pro_int(pro_int)
    assert py_int == 666

# Note that all swi floats are actually doubles.
def test_py_float_of_pro_float():
    pro_float = C.pl_float(3.2)
    py_float = conversion.py_float_of_pro_float(pro_float)
    #self.assertAlmostEqual(3.2, py_float)
    assert 3.2 == py_float

def test_py_int_of_pro_int_long():
    pro_int = C.pl_int(2**31-1)
    py_int = conversion.py_int_of_pro_int(pro_int)
    assert py_int == 2**31-1

# test atoms
def test_py_str_of_pro_atom():
    pro_atom = C.pl_atom("p")
    py_str = conversion.py_str_of_pro_atom(pro_atom)
    assert py_str == "p"

# test compound terms
def test_py_term_of_pro_term():
    # manually construct the term 'p(1, 2, 3)'

    refs = [ C.pl_ref() for x in range(3) ]
    for i in range(3):
        C.put_int(refs[i], i + 1)
    pl_cons_ftor = C.pl_compound("p", refs)

    # convert and check it worked
    py_term = conversion.py_term_of_pro_term(pl_cons_ftor)

    assert len(py_term) == 3
    assert py_term.name == "p"
    assert py_term.args == [1, 2, 3]

# test compound terms again, with nesting
def test_py_term_of_pro_term():
    # g(a, p(1, 2, 3))

    # First the p(1, 2, 3)
    p_cons_ftor = C.pl_refs(4)
    p_arg_refs = p_cons_ftor + 1
    for i in range(3):
        C.put_int(p_arg_refs + i, i + 1) # integers 1, 2, 3
    C.pl_compound("p", 3, p_arg_refs, p_cons_ftor)

    # Now g(a, ...)
    g_cons_ftor = C.pl_refs(3)
    g_arg_refs = g_cons_ftor + 1

    a_atom = C.pl_atom("a", ref=False)
    C.put_atom(g_arg_refs, a_atom)
    C.put_term(g_arg_refs + 1, p_cons_ftor)
    C.pl_compound("g", 2, g_arg_refs, g_cons_ftor)

    # convert and check it worked
    g_py_term = conversion.py_term_of_pro_term(g_cons_ftor)

    assert len(g_py_term) == 2
    assert g_py_term.name == "g"
    assert g_py_term[0] == "a"

    p_py_term = g_py_term[1]
    assert len(p_py_term) == 3
    assert p_py_term.name == "p"
    assert p_py_term.args == [1, 2, 3]

def test_py_var_of_pro_var():
    pro_var = C.pl_var()
    py_var = conversion.py_var_of_pro_var(pro_var)
    assert isinstance(py_var, Var)

# --------------------------
# Test the generic py_of_pro
# --------------------------

def test_py_of_pro_int():
    assert conversion.py_of_pro(C.pl_int(666)) ==  666

# SWIPL does not have separate double/float types
def test_py_of_pro_float():
    assert conversion.py_of_pro(C.pl_float(1.3)) == 1.3

def test_py_of_pro_int_long():
    assert conversion.py_of_pro(C.pl_int(2147483647)) == 2147483647 # 2^31-1

def test_py_of_pro_atom():
    pro_atom = C.pl_atom("foo")
    py_atom = conversion.py_of_pro(pro_atom)
    assert py_atom == "foo"

# compound terms
def test_py_of_pro_compound():
    p_cons_ftor = C.pl_refs(4)
    p_refs = p_cons_ftor + 1

    for i in range(3):
        C.put_int(p_refs + i, i + 1) # integers 1, 2, 3

    C.pl_compound("p", 3, p_refs, p_cons_ftor)

    py_term = conversion.py_of_pro(p_cons_ftor)

    assert len(py_term) == 3
    assert py_term.name == "p"
    assert py_term.args == [1, 2, 3]

def test_py_of_pro_var():
    pro_var = C.pl_var()
    py_var = conversion.py_of_pro(pro_var)
    assert isinstance(py_var, Var)

# -------------------------------
# Test the specific pro_*_of_py_*
# -------------------------------

# SWIPL treats both longs and ints as the "integer" type.
# I guess to be safe, we always get a long.
def test_pro_int_of_py_int():
    pro_int = C.pl_ref()
    conversion.pro_int_of_py_int(777, pro_int)
    assert C.get_int(pro_int) == 777

def test_pro_int_of_py_int_long():
    pro_int = C.pl_ref()
    conversion.pro_int_of_py_int(2**31-1, pro_int)
    assert C.get_int(pro_int) == 2**31-1

# All SWI floats are doubles
def test_pro_float_of_py_float():
    pro_float = C.pl_ref()
    conversion.pro_float_of_py_float(1.22, pro_float)
    assert C.get_float(pro_float) == 1.22

def test_pro_atom_of_py_str():
    pl_atom = C.pl_ref()
    conversion.pro_atom_of_py_str("zim", pl_atom)
    assert C.is_atom(pl_atom)
    assert C.get_str(pl_atom) == "zim"

def test_pro_term_of_py_term():
    py_term = CoreTerm("p", [1, 2, 3])
    pro_term = C.pl_ref()
    conversion.pro_term_of_py_term(py_term, pro_term)

    # check type
    assert C.is_compound(pro_term)

    # check name and arity
    nm_ar = C.get_name_and_arity(pro_term)
    assert nm_ar == ("p", 3)

    for i in range(3):
        assert C.get_int(C.get_arg(i + 1, pro_term)) == i + 1

# check nested terms works ok
def test_pro_term_of_py_term_nest():
    # f(g(X))
    inner_py_term = CoreTerm("g", [Var()])
    py_term = CoreTerm("f", [inner_py_term])

    pro_term = C.pl_ref()
    conversion.pro_term_of_py_term(py_term, pro_term)

    nm_ar = C.get_name_and_arity(pro_term)
    assert nm_ar == ("f", 1)

    # check name and arity of inner term
    inner_term = C.get_arg(1, pro_term)

    nm_ar2 = C.get_name_and_arity(inner_term)
    assert nm_ar2 == ("g", 1)

def test_pro_var_of_py_var():
    pro_var = C.pl_ref()
    conversion.pro_var_of_py_var(Var(), pro_var)
    assert C.is_var(pro_var)

# --------------------------
# Test the generic py_of_pro
# --------------------------

def test_pro_int_of_py_int():
    pro_int = C.pl_ref()
    conversion.pro_of_py(1337, pro_int)
    assert C.get_int(pro_int) == 1337

def test_pro_of_py_float():
    pro_float = C.pl_ref()
    conversion.pro_of_py(1.23, pro_float)
    assert C.get_float(pro_float) == 1.23

def test_pro_of_py_long():
    pro_long = C.pl_ref()
    conversion.pro_of_py(2**31-1, pro_long)
    assert C.get_int(pro_long) == 2**31-1

def test_pro_of_py_str():
    pro_atom = C.pl_ref()
    conversion.pro_of_py("zool", pro_atom)
    assert C.get_str(pro_atom) == "zool"

def test_pro_of_py_term():
    # any(tom, dick, harry)
    py_term = CoreTerm("any", ["tom", "dick", "harry"])
    pro_term = C.pl_ref()
    conversion.pro_of_py(py_term, pro_term)

    assert C.get_name_and_arity(pro_term) == ("any", 3)

    pl_args = [ C.get_str(C.get_arg(x, pro_term)) for x in range(1, 4) ]
    assert pl_args == ["tom", "dick", "harry"]

def test_pro_of_py_var():
    pro_var = C.pl_ref()
    conversion.pro_of_py(Var(C.pl_var()), pro_var)
    assert C.is_var(pro_var)
