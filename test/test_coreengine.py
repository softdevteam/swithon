# Pulled from the shared test repo
from unipycation_shared.tests.base_test_coreengine import BaseTestCoreEngine

from pytest import skip, raises

def skipmethod(self):
    skip("swithon does not support that or broken in some way")

class TestCoreEngine(BaseTestCoreEngine):
    def setup_class(cls):
        import tempfile
        (cls.fd, cls.fname) = tempfile.mkstemp(prefix="unipycation-")

    test_from_file_error = skipmethod
    test_error_in_database = skipmethod
    test_fail_in_database = skipmethod

    # As is stands, swithon is unable  to detect parse errors in the DB.
    # https://lists.iai.uni-bonn.de/pipermail/swi-prolog/2014/012159.html
    test_parse_db_incomplete = skipmethod
    test_from_file_error = skipmethod

    # Swithon does NOT support concurrent queries.
    # This is due to a limitation in SWI prolog:
    # "Note that a foreign context can have at most one active query"
    # http://www.swi-prolog.org/pldoc/man?section=foreign-create-query
    test_concurrent_queries = skipmethod

    # If the user tries to open a second concurrent query, then the old
    # CoreSolutionIterator is finalised, meaning that the query is closed.
    # We atleast check that an error is raised if the user tries to
    # use a stale iterator.
    def test_stale_iterator_queries(self):
        import uni
        from swithon import StaleSolutionIterator

        e1 = uni.CoreEngine("f(1). f(2).")
        e2 = uni.CoreEngine("g(2). g(1).")

        (X, Y) = (uni.Var(), uni.Var())
        t1 = uni.CoreTerm("f", [X])
        t2 = uni.CoreTerm("g", [Y])

        it1 = e1.query_iter(t1, [X])
        it2 = e2.query_iter(t2, [Y]) # it1 is finalised

        raises(StaleSolutionIterator, "it1.next()")
        it2.finalise()
