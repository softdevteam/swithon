# Pulled from the shared test repo
from unipycation_shared.tests.base_test_objects import BaseTestObjects
from pytest import skip


def skipmethod(self):
    skip("swithon does not support that or broken in some way")

class TestObjects(BaseTestObjects):

    # __str__ needs to be rethought
    # Perhaps call PL_get_chars(..., CVT_WRITE_CANONICAL)
    test_term_str_with_var = skipmethod
