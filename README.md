# Swithon: A CPython/SWI-Prolog Language Composition.

This is an experimental language composition mimicking the unipycation
API interface.

## Bootstrapping

To bootstrap standalone, run:

```
python2.7 bootstrap.py all
```

You will need git and the Python modules 'sh' and 'vcstools'.

If you are looking to bootstrap the other unipycation VMs too, then use the
universal bootstrapper, as found here:
https://bitbucket.org/softdevteam/unipycation-shared

## Usage

To run swithon, run `bin/swithon`.

## Tests

Run 'make test' to run unit tests.
