TESTDIR=test

all:
	@echo "This doesn't do anything. Try 'make test'"

.PHONY: clean
clean:
	find . -name '*__pycache__*' -type d | xargs rm -Rf

.PHONY: test
test:
	${PWD}/bin/swithon -m py.test -v ${TESTDIR}
