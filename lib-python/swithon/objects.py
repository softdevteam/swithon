from swithon.c import C

class CoreTerm(object):
    """ Represents a compound Prolog Term in Python """
    _name = None
    _len = None
    def __init__(self, name, args):
        assert args # non zero len
        import conversion

        comp_len = len(args)

        # XXX need to specialise on lists
        #if comp_len == 2 and name == ".":
        #    return C.pl_cons(args[0], args[1])

        # Allocate the name atom ref and the args refs in one go.
        pl_refs = C._libpl.PL_new_term_refs(comp_len + 1)
        pl_cons_ftor = pl_refs
        pl_refs = pl_refs + 1

        for i in range(comp_len):
            conversion.pro_of_py(args[i], pl_refs + i)

        C.pl_compound(name, comp_len, pl_refs, pl_cons_ftor)
        self.pl_term = pl_cons_ftor


    @classmethod
    def from_pl_term(cls, pl_term):
        inst = cls.__new__(CoreTerm)
        inst.pl_term = pl_term
        return inst

    # From a highlevel term
    @staticmethod
    def _from_term(e):
        return CoreTerm.from_pl_term(e.pl_term)

    def __len__(self):
        if self._len is None:
            self._name, self._len = C.get_name_and_arity(self.pl_term)
        return self._len

    @property
    def name(self):
        if self._name is None:
            self._name, self._len = C.get_name_and_arity(self.pl_term)
        return self._name

    def __getitem__(self, idx):
        length = len(self)
        if idx < 0:
            idx += length
        if not 0 <= idx < length:
            raise IndexError()
        pl_term = C.get_arg(idx + 1, self.pl_term)
        if pl_term == C.pl_false:
            import swithon
            raise swithon.SwithonError()
        import conversion
        return conversion.py_of_pro(pl_term)

    def __eq__(self, other):
        if not isinstance(other, CoreTerm): return False
        return C.compare(self.pl_term, other.pl_term) == 0

    def __ne__(self, other):
        return not self == other

    @property
    def args(self):
        return [ self[i] for i in range(len(self)) ]

    def __str__(self):
        # XXX use SWI's formatter
        return "%s(%s)" % (self.name, ", ".join(str(arg) for arg in self.args))

    def __repr__(self):
        return "CoreTerm(%r, %r)" % (self.name, self.args)

class Var(object):
    """ Represents a Prolog variable in Python """

    def __init__(self, pl_var=None):
        if pl_var is None:
            pl_var = C.pl_var() # fresh variable
        self.pl_var = pl_var

    def copy_ref(self):
        self.pl_var = C.copy_term_ref(self.pl_var)

