import cffi, sys

class SwiCError(Exception): pass
class NoSolution(Exception): pass

# C interface to SWI Prolog
class C(object):
    """ Abstracts the low-level interface to SWI-prolog.

    The idea here is that no-one else whould ever have to directly access C.
    You should never reach inside this object to get at the FFI or libpl.
    This allows us to do some oppertune abstraction.
    """

    CDEFS = """
    /*********
     * Macros
     *********/
    #define PL_Q_NODEBUG    ...
    #define PL_Q_CATCH_EXCEPTION ...
    #define PL_Q_NORMAL     ...

    #define CVT_WRITE_CANONICAL ...
    #define CVT_ALL         ...
    #define CVT_ATOM        ...

    #define TRUE            ...
    #define FALSE           ...

    #define PL_VARIABLE     ...
    #define PL_ATOM         ...
    #define PL_STRING       ...
    #define PL_INTEGER      ...
    #define PL_FLOAT        ...
    #define PL_TERM         ...

    /********
     * Types
     ********/
    typedef uintptr_t   functor_t;
    typedef uintptr_t   atom_t;
    typedef uintptr_t   term_t;
    typedef uintptr_t   qid_t;
    typedef uintptr_t   fid_t;

    typedef struct list_cell *      ListCell;
    typedef struct definition *     Definition;
    typedef struct sourceFile *     SourceFile;
    typedef struct module *         Module;
    typedef Module                  module_t;
    typedef struct procedure *      Procedure;
    typedef Procedure               predicate_t;

    /************
     * Functions
     ************/

    /* Interpreter Management */
    int                 PL_initialise(int argc, char *argv[]);

    /* PL_new_x */
    functor_t           PL_new_functor(atom_t f, int a);
    atom_t              PL_new_atom(const char *s);
    term_t              PL_new_term_refs(int n);
    term_t              PL_new_term_ref(void);
    module_t            PL_new_module(atom_t name);

    /* Queries */
    predicate_t         PL_predicate(const char *name, int arity,
                            const char* module);
    qid_t               PL_open_query(module_t m, int flags,
                            predicate_t pred, term_t t0);
    int                 PL_next_solution(qid_t qid);
    void                PL_close_query(qid_t qid);
    void                PL_cut_query(qid_t qid);

    /* PL_get_x */
    int                 PL_get_chars(term_t t, char **s, unsigned int flags);
    int                 PL_get_functor(term_t t, functor_t *f);
    int                 PL_get_integer(term_t t, int *i);
    int                 PL_get_atom(term_t t, atom_t *a);
    int                 PL_get_long(term_t t, long *i);
    int                 PL_get_float(term_t t, double *f);
    int                 PL_get_name_arity(term_t t, atom_t *name, int *arity);
    int                 PL_get_arg(int index, term_t t, term_t a);
    int                 PL_get_module(term_t t, module_t *module);

    /* PL_put_x */
    int                 PL_put_atom_chars(term_t t, const char *chars);
    int                 PL_put_integer(term_t t, long i);
    int                 PL_put_float(term_t t, double f);
    int                 PL_put_atom(term_t t, atom_t a);
    int                 PL_put_variable(term_t t);
    int                 PL_put_term(term_t t1, term_t t2);
    int                 PL_put_functor(term_t t, functor_t functor);
    int                 PL_put_nil(term_t l);

    /* Exceptions */
    term_t              PL_exception(qid_t qid);

    /* Unification */
    int                 PL_unify(term_t t1, term_t t2);

    /* Type checks */
    int                 PL_is_integer(term_t t);
    int                 PL_is_float(term_t t);
    int                 PL_is_atom(term_t t);
    int                 PL_is_compound(term_t t);
    int                 PL_is_variable(term_t t);
    int                 PL_term_type(term_t);

    /* Frames */
    fid_t               PL_open_foreign_frame(void);
    void                PL_close_foreign_frame(fid_t cid);
    void                PL_discard_foreign_frame(fid_t id);

    /* Misc */
    int                 PL_chars_to_term(const char *chars, term_t term);
    int                 PL_cons_functor(term_t h, functor_t f, ...);
    int                 PL_cons_functor_v(term_t h, functor_t f, term_t a0);
    void                PL_register_atom(atom_t atom);
    const char *        PL_atom_chars(atom_t atom);
    int                 PL_cons_list(term_t l, term_t h, term_t t);
    term_t              PL_copy_term_ref(term_t from);
    int                 PL_compare(term_t t1, term_t t2);
    atom_t              PL_module_name(module_t module);

    /* own code */
    int                 cons_functor_from_name_and_arity(term_t h, const char *s, int a, term_t a0);
    qid_t               open_query_with_term(term_t c, module_t m, int flags);
    """

    @staticmethod
    def _call_pkgconfig(argstr):
        import subprocess
        args = "pkg-config " + argstr
        pipe = subprocess.Popen(args, shell=True, stdout=subprocess.PIPE)
        stdout = pipe.communicate()[0]
        assert pipe.returncode == 0
        return stdout

    def __init__(self):
        cflags = C._call_pkgconfig("--cflags swipl").split()
        ldflags = C._call_pkgconfig("--libs swipl").split()

        ldflags += ["-ltermcap"]

        # Initialise CFFI
        self._ffi = cffi.FFI()
        self._ffi.cdef(C.CDEFS)
        self._libpl = self._ffi.verify("""
            #include <stdio.h>
            #include <string.h>
            #include <SWI-Prolog.h>
            #include <SWI-Stream.h>

            /*
             * This is how we consult a string.
             * Thanks to Jan Wielemaker for this suggestion.
             */
            int
            open_stream(char *s, term_t t)
            {
                IOSTREAM *stream;

                stream = Sopen_string(NULL, strdup(s), strlen(s), "r");
                //stream->encoding = ENC_UTF8;

                return PL_unify_stream(t, stream);
            }


            int
            cons_functor_from_name_and_arity(term_t h, const char *s, int a, term_t a0)
            {
                atom_t atom;
                functor_t functor;
                atom = PL_new_atom(s);
                functor = PL_new_functor(atom, a);
                return PL_cons_functor_v(h, functor, a0);
            }

            qid_t
            open_query_with_term(term_t c, module_t m, int flags) {
                functor_t functor;
                atom_t ignored;
                int arity, n;
                term_t a0;
                predicate_t pred;

                PL_get_functor(c, &functor);
                PL_get_name_arity(c, &ignored, &arity);

                pred = PL_pred(functor, m);

                a0 = PL_new_term_refs(arity);
                for (n = 0; n < arity; n++) {
                    PL_get_arg(n+1, c, a0+n);
                }

                return PL_open_query(m, flags, pred, a0);
            }
        """,    #library_dirs=[SWI_LIBDIR],
                #libraries=["pl"],
                #include_dirs=[SWI_INCDIR])
                extra_compile_args=cflags,
                extra_link_args=ldflags,
            )
        self.pl_false = self._libpl.FALSE
        self.pl_true = self._libpl.TRUE

        # initialise SWI
        self._libpl.PL_initialise(1, [self._ffi.new("char[]", sys.executable)])

        # Shared pointers
        self.int_ptr = self._ffi.new("int*")
        self.long_ptr = self._ffi.new("long*")
        self.module_t_ptr = self._ffi.new("module_t*")
        self.double_ptr = self._ffi.new("double*")
        self.char_ptr_ptr = self._ffi.new("char**")
        self.atom_t_ptr = self._ffi.new("atom_t*")

    def compare(self, t1, t2): return self._libpl.PL_compare(t1, t2)

    def mk_list(self, pl_items):
        a = self.pl_ref()
        l = self.pl_ref()
        self._libpl.PL_put_nil(l)

        rev_pl_items = list(reversed(pl_items))
        for i in rev_pl_items:
            self.put_term(a, i)
            self._libpl.PL_cons_list(l, a, l)

        return l

    def copy_term_ref(self, ref):
        return self._libpl.PL_copy_term_ref(ref)

    def next_solution(self, pl_qid):
        """ Assign the next binding (if there is one) """
        pl_sol = self._libpl.PL_next_solution(pl_qid);

        pl_exc = self._libpl.PL_exception(pl_qid)
        if pl_sol is self._libpl.FALSE and pl_exc != 0:
            # A prolog error occured
            raise SwiCError(self.get_str(pl_exc))
        elif pl_sol is self.pl_false:
            raise NoSolution()

    def close_query(self, pl_qid):
        self._libpl.PL_close_query(pl_qid)

    def cut_query(self, pl_qid):
        self._libpl.PL_cut_query(pl_qid)

    def make_c_module(self, module_name):
        return self._libpl.PL_new_module(self.pl_atom(module_name, ref=False))


    def open_query_with_term(self, pl_term, pl_mod=None):
        return self._libpl.open_query_with_term(pl_term, pl_mod, self._libpl.PL_Q_NODEBUG)


    # Args should be Prolog term refs already
    def open_query(self, pred_name, arity, args, c_mod_name=None):

        # Construct callable predicate name/arity
        c_pred_name = self._ffi.new("char[]", pred_name)

        if module_name is None:
            c_mod_name = self._ffi.NULL

        pl_pred = self._libpl.PL_predicate(c_pred_name, arity, c_mod_name)

        # Args
        pl_args = self._libpl.PL_new_term_refs(len(args))
        for i in range(len(args)):
            self.put_term(pl_args + i, args[i])

        # Now open the query within the prolog interpreter
        pl_qid = self._libpl.PL_open_query(
                self._ffi.NULL, self._libpl.PL_Q_NODEBUG, pl_pred, pl_args)

        return pl_qid

    def open_frame(self):
        return self._libpl.PL_open_foreign_frame()

    def close_frame(self, pl_fid):
        return self._libpl.PL_close_foreign_frame(pl_fid)

    def discard_frame(self, pl_fid):
        return self._libpl.PL_discard_foreign_frame(pl_fid)

    # convinience methods for data construction
    def pl_int(self, num):
        pl_term = self.pl_ref()
        # Note this actually puts a long
        self._libpl.PL_put_integer(pl_term, num)
        return pl_term

    def pl_float(self, num):
        pl_term = self.pl_ref()
        self._libpl.PL_put_float(pl_term, num)
        return pl_term

    # mainly for debugging
    def pl_type_name(self, t):
        f = self._libpl
        type_names = {
                f.PL_VARIABLE : "variable",
                f.PL_ATOM : "atom",
                f.PL_STRING : "string",
                f.PL_INTEGER : "integer",
                f.PL_FLOAT : "float",
                f.PL_TERM : "term",
        }

        try:
            return type_names[self._libpl.PL_term_type(t)]
        except KeyError:
            raise SwiCError("This is not a valid proog term ref: %s" % t)

    def _cast_pl_for_ffi_va(self, pl_data):
        if self.is_atom(pl_data):
            return self._ffi.cast("atom_t", pl_data)
        elif self.is_compound(pl_data) or \
                self.is_int(pl_data) or \
                self.is_var(pl_data) or \
                self.is_float(pl_data):
            return self._ffi.cast("term_t", pl_data)
        else:
            # Bug in SwiPL 6.x
            # Blobs are reported as atoms by PL_term_type().
            # Note that blobs are not reported as an atom by PL_is_atom().
            # Fixed in 7.x but not going to be backported to 6.x.
            if self._libpl.PL_term_type(pl_data) == self._libpl.PL_ATOM:
                return self._ffi.cast("term_t", pl_data)

            # Otherwise we have not yet handled that
            raise NotImplementedError(self.pl_type_name(pl_data))

    # helper abstraction
    def pl_compound(self, funct_name, comp_len, pl_args, pl_cons_ftor):
        self._libpl.cons_functor_from_name_and_arity(
                pl_cons_ftor, funct_name, comp_len, pl_args)

    def pl_cons(self, pl_head, pl_tail):
        pl_cons = self.pl_ref()
        self._libpl.PL_cons_list(pl_cons, pl_head, pl_tail)
        return pl_cons


    def pl_atom(self, name, ref=True):
        # if ref=True you get a reference to an atom
        # otherwise you get the raw atom
        atom = self._libpl.PL_new_atom(name)
        if not ref: return atom

        ref = self.pl_ref()
        self._libpl.PL_put_atom(ref, atom)
        return ref

    # A variable and a reference are the same thing, but sometimes it makes
    # sense to refer to them in one way or another, hence two identical meths.
    def pl_var(self): return self._libpl.PL_new_term_ref()
    pl_ref = pl_var
    def pl_refs(self, n): return self._libpl.PL_new_term_refs(n)

    # ---------------------------------------------------------------
    # Convinience methods for data extraction (from prolog term refs)
    # ---------------------------------------------------------------

    def get_module_from_str(self, name):
        pl_atom_ref = self.pl_atom(name, ref=True)
        self._libpl.PL_get_module(pl_atom_ref, self.module_t_ptr)
        return self.module_t_ptr[0]

    # Mostly for debugging
    def get_module_name(self, mod_t):
        nm_atom = self._libpl.PL_module_name(mod_t)
        nm = self._libpl.PL_atom_chars(nm_atom)
        nm_pystr = self._ffi.string(nm)
        return nm_pystr

    def get_int(self, ref):
        self._libpl.PL_get_long(ref, self.long_ptr)
        return self.long_ptr[0]

    def get_float(self, ref):
        self._libpl.PL_get_float(ref, self.double_ptr)
        return self.double_ptr[0]

    def get_str(self, ref):
        self._libpl.PL_get_chars(ref, self.char_ptr_ptr, self._libpl.CVT_WRITE_CANONICAL)
        return self._ffi.string(self.char_ptr_ptr[0])

    # Applies to atom refs and composite refs
    def get_name_and_arity(self, ref):
        self._libpl.PL_get_name_arity(ref, self.atom_t_ptr, self.int_ptr)
        c_name = self._libpl.PL_atom_chars(self.atom_t_ptr[0])
        return (self._ffi.string(c_name), self.int_ptr[0])

    def get_atom(self, t):
        self._libpl.PL_get_atom(t, self.atom_t_ptr)
        return self.atom_t_ptr[0]

    # Abstract get_*
    def get_name(self, ref): return self.get_name_and_arity(ref)[0]
    def get_arity(self, ref): return self.get_name_and_arity(ref)[1]

    def get_arg(self, index, comp_ref):
        pl_term = self.pl_ref()
        self._libpl.PL_get_arg(index, comp_ref, pl_term)
        return pl_term

    # Abstract put_*
    def put_int(self, ref, i): self._libpl.PL_put_integer(ref, i)
    def put_float(self, ref, i): self._libpl.PL_put_float(ref, i)
    def put_atom(self, ref, atom_t): self._libpl.PL_put_atom(ref, atom_t)
    def put_var(self, ref, v): self._libpl.PL_put_variable(ref, v)
    def put_term(self, ref, t): self._libpl.PL_put_term(ref, t)

    # Abstract type checks
    def is_atom(self, t): return self._libpl.PL_is_atom(t) != 0
    def is_int(self, t): return self._libpl.PL_is_integer(t) != 0
    def is_float(self, t): return self._libpl.PL_is_float(t) != 0
    def is_compound(self, t): return self._libpl.PL_is_compound(t) != 0
    def is_var(self, t): return self._libpl.PL_is_variable(t) != 0

    # A module variable which is shared across the entire project.

C = C()
