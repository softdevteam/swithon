from swithon.objects import CoreTerm, Var
from swithon.c import C
from debug import debugf, debug

# -------------------------
# Prolog Data to Pyton Data
# -------------------------

def py_int_of_pro_int(pro_int): return C.get_int(pro_int)
def py_float_of_pro_float(pro_float): return C.get_float(pro_float)
def py_str_of_pro_atom(pro_atom): return C.get_str(pro_atom)

# Composite terms
def py_term_of_pro_term(pro_term):
    return CoreTerm.from_pl_term(pro_term)

def py_var_of_pro_var(pro_var):
    return Var(pro_var)

# Top-level abstracted conversion: pro -> py
def py_of_pro(pro_term):
    lib = C._libpl
    pl_type = lib.PL_term_type(pro_term)
    if pl_type == lib.PL_INTEGER:
        return py_int_of_pro_int(pro_term)
    if pl_type == lib.PL_FLOAT:
         return py_float_of_pro_float(pro_term)
    if pl_type == lib.PL_ATOM:
         return py_str_of_pro_atom(pro_term)
    if pl_type == lib.PL_TERM:
         return py_term_of_pro_term(pro_term)
    if pl_type == lib.PL_VARIABLE:
         return py_var_of_pro_var(pro_term)
    else:
        raise NotImplementedError(str(pro_term))

# --------------------------
# Python Data to Prolog Data
# --------------------------

def pro_int_of_py_int(py_int, ref):
    C._libpl.PL_put_integer(ref, py_int)

def pro_float_of_py_float(py_float, ref):
    C._libpl.PL_put_float(ref, py_float)

def pro_var_of_py_var(py_var, ref):
    C.put_term(ref, py_var.pl_var)

def pro_atom_of_py_str(py_str, ref):
    atom = C._libpl.PL_new_atom(py_str)
    C._libpl.PL_put_atom(ref, atom)
    #return C.pl_atom(py_str)

def pro_term_of_py_term(py_term, ref):
    C.put_term(ref, py_term.pl_term)

def pro_of_py(py_data, ref):
    typ = type(py_data)
    if typ is int:
        pro_int_of_py_int(py_data, ref)
    elif typ is long:
        pro_int_of_py_int(py_data, ref)
    elif typ is str:
        pro_atom_of_py_str(py_data, ref)
    elif typ is float:
        pro_float_of_py_float(py_data, ref)
    elif typ is Var:
        pro_var_of_py_var(py_data, ref)
    elif typ is CoreTerm:
        pro_term_of_py_term(py_data, ref)
    else:
        pro_of_py_slow(py_data, ref)

def pro_of_py_slow(py_data, ref):
    if isinstance(py_data, int):
        pro_int_of_py_int(py_data, ref)
    elif isinstance(py_data, long):
        pro_int_of_py_int(py_data, ref)
    elif isinstance(py_data, str):
        pro_atom_of_py_str(py_data, ref)
    elif isinstance(py_data, float):
        pro_float_of_py_float(py_data, ref)
    elif isinstance(py_data, Var):
        pro_var_of_py_var(py_data, ref)
    elif isinstance(py_data, CoreTerm):
        pro_term_of_py_term(py_data, ref)
    else:
        raise NotImplementedError(py_data)
