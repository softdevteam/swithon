from objects import Var, CoreTerm
from core_engine import CoreEngine

class SwithonError(Exception): pass # XXX used?
class PrologError(Exception): pass
class ParseError(Exception): pass # Not used yet but needed for tests XXX
class StaleSolutionIterator(Exception): pass
