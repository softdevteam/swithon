import tempfile, os, os.path

from swithon import Var
from swithon.c import C, NoSolution, SwiCError
from swithon import conversion
from conversion import py_of_pro, pro_of_py
from swithon.objects import CoreTerm, Var

class CoreSolutionIterator(object):

    # We stash the last CoreSolutionIterator so that we can check
    # if it was finalised properly. Remember, SWI prolog only allows
    # one query to be open at a time.
    _current_iterator = None


    # we always have a frame open. When a CoreSolutionIterator.next is called,
    # the iterator takes ownership of the frame, and later discards it and
    # makes a new one.

    _open_frame = C.open_frame()

    def __init__(self, core_engine, goal, free_vars, pl_mod=None):

        if not isinstance(goal, CoreTerm):
            raise TypeError("goal must be a term")

        if not all(isinstance(x, Var) for x in free_vars):
            raise TypeError("free vars must be list of vars")

        # Finalise any previous iterator
        cur_iter = CoreSolutionIterator._current_iterator
        if cur_iter is not None and not cur_iter.finalised:
            cur_iter.finalise() # iterator invalidated
        CoreSolutionIterator._current_iterator = self

        self.pl_mod = pl_mod
        self.finalised = False
        self.core_engine = core_engine
        self.goal = goal
        self.free_vars = free_vars
        self.pl_qid = None # query ID, also indicates if next() was called
        self.pl_fid = None # frame ID

    def __iter__(self): return self

    def finalise(self):
        if self.finalised: return
        # Only close a query if one was opened (by a call to next())
        if self.pl_qid is not None:
            C.close_query(self.pl_qid)
            C.discard_frame(self.pl_fid)
            CoreSolutionIterator._open_frame = C.open_frame()
            (self.pl_qid, self.pl_fid) = (None, None)
        self.finalised = True

    def next(self):
        if self.finalised:
            from swithon import StaleSolutionIterator
            raise StaleSolutionIterator("Finalised iterator")

        # first iteration sets up the query
        if self.pl_qid is None:

            self.pl_fid = CoreSolutionIterator._open_frame
            assert self.pl_fid is not None
            CoreSolutionIterator._open_frame = None
            self.pl_qid = C.open_query_with_term(self.goal.pl_term, self.pl_mod)

            try:
                C.next_solution(self.pl_qid)
            except NoSolution:
                self.finalise()
                raise StopIteration # Never was a solution
            except SwiCError as e: # usually nonexistent predicate
                from swithon import PrologError
                raise PrologError(e.args[0])
        else:
            try:
                C.next_solution(self.pl_qid)
            except NoSolution:
                self.finalise()
                raise StopIteration # No more solutions

        return Solution(self.free_vars)

class CoreEngine(object):

    # Each CoreEngine has its own module so that predicates are
    # not shared between CoreEngine instances.
    MODULE_PREFIX = "swithon__internal__"
    MODULE_SUFFIX = 0 # incremented upon each new Engine

    def _mk_module_name(self):
        n = CoreEngine.MODULE_PREFIX + str(CoreEngine.MODULE_SUFFIX)
        CoreEngine.MODULE_SUFFIX += 1
        return n

    def _consult_file(self, filename):
        import os.path
        t = CoreTerm("consult", [filename])
        it = self._query_iter_module(t, [], "user")
        it.next()
        it.finalise()

    def __init__(self, pl_code):
        # Generate module for this CoreEngine's namespace
        self.module_name = self._mk_module_name()
        self.pl_mod = C.make_c_module(self.module_name)
        with tempfile.NamedTemporaryFile(dir="/tmp") as pl_file:
            pl_code = (":- module(%s, []).\n" % self.module_name) + pl_code
            pl_file.write(pl_code)
            pl_file.flush()
            self._consult_file(pl_file.name)

    @classmethod
    def from_file(cls, filename):
        with open(filename, "r") as fh:
            src = fh.read()
        return cls(src)

    def query_iter(self, goal, free_vars):
        return CoreSolutionIterator(self, goal, free_vars, self.pl_mod)

    # Allows you to specify which module, internal only!!!
    def _query_iter_module(self, goal, free_vars, module_name):
        pl_mod = C.make_c_module(module_name)
        return CoreSolutionIterator(self, goal, free_vars, pl_mod)

class Solution(object):
    """ A wrapper around a solution. """

    def __init__(self, unbound_vars):
        self.unbound_vars = unbound_vars
        self.result_dict = None

        # We need to do this now otherwise we may get the
        # bindings for a future solution
        self._create_dict()

    def _create_dict(self):
        if self.result_dict is None:
            # copy_term_ref prevents PL_close_query from deallocating the binding
            self.result_dict = \
                { v : py_of_pro(C.copy_term_ref(v.pl_var)) for v in self.unbound_vars }

    def __len__(self): return len(self.unbound_vars)

    # used by explicit interface
    def __getitem__(self, key):
        #self._create_dict() # always done by this point
        return self.result_dict[key]

    # used by abstracted interface
    def get_values_in_order(self):
        return tuple([ self[v] for v in self.unbound_vars ])

    # for debugging only
    def __str__(self):
        self._create_dict()
        return str(self.result_dict)
