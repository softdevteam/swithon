def debugm(func):
    import inspect
    def wrap(self, *args, **kwargs):
        print(" [ >>> DEBUG METHOD ] %s.%s: caller=%s args=%s  kwargs=%s" %
            (type(self).__name__, func.__name__, inspect.stack()[1][3], args, kwargs))
        rv = func(self, *args, **kwargs)
        print(" [ <<< DEBUG METHOD ] ret=%s" % str(rv))
        return rv
    return wrap

def debugf(func):
    def wrap(*args, **kwargs):
        print(" [ DEBUG FUNCTION ] %s: args=%s  kwargs=%s" %
            (func.__name__, args, kwargs))
        rv = func(*args, **kwargs)
        print(" [ >>> DEBUG METHOD ] ret=%s" % rv)
        return rv
    return wrap

def debug(msg):
    print(" [ DEBUG MESSAGE ]: %s" % str(msg))
