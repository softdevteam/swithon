#!/usr/bin/env python2.7
import os, os.path, sys, importlib, urllib, sh

SCRIPT_PATH = os.path.abspath(__file__)
SCRIPT_DIR = os.path.dirname(SCRIPT_PATH)
DEPS_DIR = os.path.join(SCRIPT_DIR, "deps")
SWITHON_BIN_DIR = os.path.join(SCRIPT_DIR, "bin")
UNI_SYMLINK_DIR = os.path.join(SCRIPT_DIR, "lib-python")

SWI_V = "6.6.1"
SWI_TARBALL_FILE = "pl-%s.tar.gz" % SWI_V
SWI_TARBALL_URL = "http://www.swi-prolog.org/download/stable/src/%s" % \
    SWI_TARBALL_FILE
SWI_TARBALL_PATH = os.path.join(DEPS_DIR, SWI_TARBALL_FILE)
SWI_DIR = os.path.join(DEPS_DIR, "pl-%s" % SWI_V)
SWI_INST_DIR = os.path.join(SWI_DIR, "inst")

CPY_V = "2.7.6"
CPY_TARBALL_FILE = "Python-%s.tgz" % CPY_V
CPY_EXTRACTED_DIR = "Python-%s" % CPY_V
CPY_TARBALL_URL = "https://www.python.org/ftp/python/%s/%s" % \
        (CPY_V, CPY_TARBALL_FILE)
CPY_TARBALL_PATH = os.path.join(DEPS_DIR, CPY_TARBALL_FILE)
CPY_DIR = os.path.join(DEPS_DIR, CPY_EXTRACTED_DIR)
CPY_INST_DIR = os.path.join(CPY_DIR, "inst")
CPY_BIN = os.path.join(CPY_INST_DIR, "bin", "python2.7")

SETUPTOOLS_V = "3.4.1"
SETUPTOOLS_TARBALL_FILE = "setuptools-%s.tar.gz" % SETUPTOOLS_V
SETUPTOOLS_EXTRACTED_DIR = "setuptools-%s" % SETUPTOOLS_V
SETUPTOOLS_TARBALL_URL = "https://pypi.python.org/packages/source/s/" + \
        "setuptools/%s" % SETUPTOOLS_TARBALL_FILE
SETUPTOOLS_DIR = os.path.join(DEPS_DIR, SETUPTOOLS_EXTRACTED_DIR)
SETUPTOOLS_TARBALL_PATH = os.path.join(DEPS_DIR, SETUPTOOLS_TARBALL_FILE)
SETUPTOOLS_SETUP_PY = os.path.join(SETUPTOOLS_DIR, "setup.py")

CFFI_V = "0.8.2"
CFFI_TARBALL_FILE = "cffi-%s.tar.gz" % CFFI_V
CFFI_EXTRACTED_DIR = "cffi-%s" % CFFI_V
CFFI_DIR = os.path.join(DEPS_DIR, CFFI_EXTRACTED_DIR)
CFFI_TARBALL_URL = "https://pypi.python.org/packages/source/c/cffi/%s" % \
        CFFI_TARBALL_FILE
CFFI_TARBALL_PATH = os.path.join(DEPS_DIR, CFFI_TARBALL_FILE)
CFFI_SETUP_PY = os.path.join(CFFI_DIR, "setup.py")

PYTEST_V = "2.5.2"
PYTEST_TARBALL_FILE = "pytest-%s.tar.gz" % PYTEST_V
PYTEST_EXTRACTED_DIR = "pytest-%s" % PYTEST_V
PYTEST_DIR = os.path.join(DEPS_DIR, PYTEST_EXTRACTED_DIR)
PYTEST_TARBALL_URL = "https://pypi.python.org/packages/source/p/pytest/%s" % \
        PYTEST_TARBALL_FILE
PYTEST_TARBALL_PATH = os.path.join(DEPS_DIR, PYTEST_TARBALL_FILE)
PYTEST_SETUP_PY = os.path.join(PYTEST_DIR, "setup.py")

SHARED_VCS = "git"
SHARED_VERSION = "master"
SHARED_REPO = "git@bitbucket.org:softdevteam/unipycation-shared.git"
DEFAULT_SHARED_DIR = os.path.join(DEPS_DIR, "unipycation-shared")

PATHS_CONF = os.path.join(SCRIPT_DIR, "paths.conf")

#
# FETCHING
#

def fetch_deps(with_shared=True):
    if not os.path.exists(DEPS_DIR):
        os.mkdir(DEPS_DIR)

    if with_shared: fetch_shared()

    # Prolog
    fetch_swi()

    # Python
    fetch_cpython()
    fetch_setuptools()
    fetch_cffi()
    fetch_pytest()

def fetch_swi():
    if not os.path.exists(SWI_TARBALL_PATH):
        print("Fetching %s..." % SWI_TARBALL_FILE)
        urllib.urlretrieve(SWI_TARBALL_URL, SWI_TARBALL_PATH)

def fetch_cpython():
    if not os.path.exists(CPY_TARBALL_PATH):
        print("Fetching %s..." % CPY_TARBALL_FILE)
        urllib.urlretrieve(CPY_TARBALL_URL, CPY_TARBALL_PATH)

def fetch_setuptools():
    if not os.path.exists(SETUPTOOLS_TARBALL_PATH):
        print("Fetching %s..." % SETUPTOOLS_TARBALL_FILE)
        urllib.urlretrieve(SETUPTOOLS_TARBALL_URL, SETUPTOOLS_TARBALL_PATH)

def fetch_cffi():
    if not os.path.exists(CFFI_TARBALL_PATH):
        print("Fetching %s..." % CFFI_TARBALL_FILE)
        urllib.urlretrieve(CFFI_TARBALL_URL, CFFI_TARBALL_PATH)

def fetch_pytest():
    if not os.path.exists(PYTEST_TARBALL_PATH):
        print("Fetching %s..." % PYTEST_TARBALL_FILE)
        urllib.urlretrieve(PYTEST_TARBALL_URL, PYTEST_TARBALL_PATH)

# used only for standalone bootstrap
def fetch_shared():
    import vcstools
    vcs = vcstools.get_vcs_client(SHARED_VCS, DEFAULT_SHARED_DIR)
    if not os.path.exists(DEFAULT_SHARED_DIR):
        print("Cloning fresh unipycation-shared: version=%s" % SHARED_VERSION)
        vcs.checkout(SHARED_REPO, version=SHARED_VERSION)
    else:
        print("Updating existing unipycation-shared to version: %s"
                % SHARED_VERSION)
        vcs.update(version=SHARED_VERSION, force_fetch=True)

#
# BUILDING
#

def make(target="all"):
    try:
        sh.gmake(target, _out=sys.stderr, _err=sys.stderr) # BSD
    except sh.CommandNotFound:
        try:
            sh.make(target, _out=sys.stderr, _err=sys.stderr) # linux
        except sh.CommandNotFound:
            raise CommandNotFound("Couldn't find gmake")

def build_deps():
    # Python
    build_cpython()
    build_setuptools()
    build_cffi()
    build_pytest()

    # Prolog
    build_swi()

def build_swi():
    if not os.path.exists(SWI_DIR):
        os.chdir(DEPS_DIR)
        print("Extracting %s..." % SWI_TARBALL_FILE)
        sh.tar("zxf", SWI_TARBALL_FILE)

    if not os.path.exists(SWI_INST_DIR):
        print("Building SWI Prolog...")
        os.chdir(SWI_DIR)
        sh.Command("./configure")("--prefix=%s" % SWI_INST_DIR,
                _out=sys.stdout, _err=sys.stderr)
        make()
        make("install")

def build_cpython():
    if not os.path.exists(CPY_DIR):
        os.chdir(DEPS_DIR)
        print("Extracting %s..." % CPY_TARBALL_FILE)
        sh.tar("zxf", CPY_TARBALL_FILE)

    if not os.path.exists(CPY_INST_DIR):
        print("Building CPython...")
        os.chdir(CPY_DIR)

        # needed to pick up c++
        if sys.platform.startswith("openbsd"):
            os.environ["CXX"] = "g++"

	args = ["--prefix=%s" % CPY_INST_DIR]

        if sys.platform.startswith("linux"):
            args += ["--enable-unicode=ucs4"]

        sh.Command("./configure")(*args, _out=sys.stdout, _err=sys.stderr)
        make()
        make("install")

def build_setuptools():
    if not os.path.exists(SETUPTOOLS_DIR):
        os.chdir(DEPS_DIR)
        print("Extracting %s..." % SETUPTOOLS_TARBALL_FILE)
        sh.tar("zxf", SETUPTOOLS_TARBALL_FILE)

    try:
        sh.Command(CPY_BIN)("-c", "import easy_install")
    except sh.ErrorReturnCode:
        os.chdir(SETUPTOOLS_DIR) # or build junk goes in cwd
        print("Installing setuptools...")
        sh.Command(CPY_BIN)(SETUPTOOLS_SETUP_PY, "install",
                _out=sys.stdout, _err=sys.stderr)

def build_cffi():
    if not os.path.exists(CFFI_DIR):
        print("Extracting %s..." % CFFI_TARBALL_FILE)
        os.chdir(DEPS_DIR)
        sh.tar("zxf", CFFI_TARBALL_FILE)

    try:
        sh.Command(CPY_BIN)("-c", "import cffi")
    except sh.ErrorReturnCode:
        print("Installing cffi...")
        os.chdir(CFFI_DIR)
        sh.Command(CPY_BIN)(CFFI_SETUP_PY, "install",
                _out=sys.stdout, _err=sys.stderr)

def build_cffi_cache():
    print("Building CFFI cache...")
    sh.Command(os.path.join(SWITHON_BIN_DIR, "swithon"))(
            "-c", '__import__("uni")', _out=sys.stdout, _err=sys.stderr)

def build_pytest():
    if not os.path.exists(PYTEST_DIR):
        print("Extracting %s..." % PYTEST_TARBALL_FILE)
        os.chdir(DEPS_DIR)
        sh.tar("zxf", PYTEST_TARBALL_FILE)

    try:
        sh.Command(CPY_BIN)("-c", "import pytest")
    except sh.ErrorReturnCode:
        print("Installing pytest...")
        os.chdir(PYTEST_DIR)
        sh.Command(CPY_BIN)(PYTEST_SETUP_PY, "install",
                _out=sys.stdout, _err=sys.stderr)

#
# CONFIGURATION
#

def configure(shared_dir=DEFAULT_SHARED_DIR):
    gen_paths_dot_conf(shared_dir)
    gen_uni_symlink(shared_dir)

def gen_paths_dot_conf(shared_dir):
        print("Generating paths.conf...")
        os.chdir(SCRIPT_DIR)
        with open(PATHS_CONF, "w") as conf:
            conf.write("SHARED_PATH=%s\n" % shared_dir)
            conf.write("SWI_PATH=%s\n" % SWI_DIR)
            conf.write("CPY_BIN=%s\n" % CPY_BIN)

def force_symlink(src, dest):
    if os.path.islink(dest):
        os.unlink(dest)
    os.symlink(src, dest)

def gen_uni_symlink(shared_dir):
    print("Generating uni.py symlink...")
    uni_py_path = os.path.join(shared_dir, "unipycation_shared", "uni.py")
    target_path = os.path.join(UNI_SYMLINK_DIR, "uni.py")

    force_symlink(uni_py_path, target_path)

    # Remove old bytecode if there is one
    pyc_path = target_path + "c"
    try:
        os.unlink(pyc_path)
    except OSError:
        pass

#
# MAIN
#

def bootstrap(target, shared_dir=None):
    if shared_dir is None:
        with_shared = True
        shared_dir = DEFAULT_SHARED_DIR
    else:
        with_shared = False
        shared_dir = os.path.abspath(shared_dir)

    if target in ["fetch", "all"]:
        fetch_deps(with_shared)

    # always configure
    configure(shared_dir)

    if target in ["build", "all"]:
        build_deps()
	build_cffi_cache()

#
# MAIN
#

def usage():
    print("\nUsage:")
    print("  bootstrap.py target [unipycation_shared_path]")
    print("\nIf no path specified, will clone afresh.")
    print("Valid targets are: fetch, build, configure, all")
    sys.exit(1)

if __name__ == "__main__":
    try:
        shared_arg = sys.argv[2]
    except IndexError:
        shared_arg = None

    try:
        target = sys.argv[1]
    except IndexError:
        usage()

    if target not in ["fetch", "build", "configure", "all"]:
        print("Bad target")
        usage()

    if shared_arg is not None:
        bootstrap(target, shared_arg)
    else:
        bootstrap(target)
